'use strict';

var App = {

    init: function() {
        this.todos = this.fetch();
        this.render(this.todos);
        this.bindEvents();
    },

    fetch: function() {
        var todos = localStorage.getItem('todos');

        if (todos) {
            return JSON.parse(todos);
        } else {
            return [];
        }
    },

    // Events

    bindEvents: function() {
        var that = this,
            add_btn = document.getElementById('todo_add'),
            list = document.getElementById('todo_list'),
            drags = document.querySelectorAll('.drag'),
            search_btn = document.getElementById('todo_search'),
            clear_btn = document.getElementById('clear_btn'),
            check_btn = document.getElementById('check_active');

        add_btn.addEventListener('click', this.create.bind(this));
        search_btn.addEventListener('click', this.search.bind(this));
        clear_btn.addEventListener('click', this.clear.bind(this));
        check_btn.addEventListener('click', this.toggleActive.bind(this));

        list.addEventListener('click', function(e) {
            var inner = e.target.innerHTML,
                targetId = e.target.getAttribute('id'),
                index = this.getIndex(targetId);

            if (inner === 'clear') {
                this.remove(index);
            } else if (inner === 'check_box' || 'check_box_outline_blank') {
                this.toggle(index);
            }
        }.bind(this));

        [].forEach.call(drags, function(drag) {
            drag.addEventListener('dragstart', that.handleDragStart.bind(that), false);
            drag.addEventListener('dragenter', that.handleDragEnter.bind(that), false);
            drag.addEventListener('dragover', that.handleDragOver.bind(that), false);
            drag.addEventListener('drop', that.handleDragEnd.bind(that), false);
        });
    },

    unbindAllEvents: function() {
        var add_btn = document.getElementById('todo_add'),
            list = document.getElementById('todo_list'),
            search_btn = document.getElementById('todo_search'),
            clear_btn = document.getElementById('clear_btn'),
            check_btn = document.getElementById('check_active');

        this.unbindEvent(add_btn);
        this.unbindEvent(list);
        this.unbindEvent(search_btn);
        this.unbindEvent(clear_btn);
        this.unbindEvent(check_btn);
    },

    unbindEvent: function(elem) {
        var clone = elem.cloneNode(true);
        elem.parentNode.replaceChild(clone, elem);
    },

    saveAndUpdateEvents: function(todos) {
        this.save();
        this.render(todos);

        this.unbindAllEvents();
        this.bindEvents();
    },

    // HTML5 Drag'n'Drop Events

    handleDragStart: function(e) {
        e.dataTransfer.effectAllowed = 'move';
        e.dataTransfer.setData('Text', e.target.getAttribute('id'));
        return true;
    },
    handleDragEnter: function(e) {
        e.preventDefault();
    },
    handleDragOver: function(e) {
        e.preventDefault();
    },
    handleDragEnd: function(e) {
        var data = e.dataTransfer.getData('Text');
        var target = e.target
        var elem = document.getElementById(data)
        target.parentNode.insertBefore(elem, target.nextSibling);
        this.savePositions();
        e.stopPropagation();
        return false;
    },

    savePositions: function() {
        var that = this;
        var listItemsIds = document.getElementById('todo_list').children;
        var ids = Object.keys(listItemsIds).map(function(key) {
            if (key.includes('todo_item-')) {
                return key.split('-')[1];
            }
        })
        ids.splice(0, ids.length / 2);

        ids.forEach(function(item) {
            that.todos.sort(function(todo) {
                return todo.id.includes(item);
            });
        });
        this.saveAndUpdateEvents();
    },

    create: function() {
        var inputValue = document.getElementById('todo_input').value;

        if (inputValue === '') {
            return
        }
        this.todos.push({
            title: inputValue.trim(),
            completed: false,
            id: this.uuid()
        });
        document.getElementById('todo_input').value = '';

        this.saveAndUpdateEvents(this.todos);
    },

    remove: function(index) {
        if (index === -1) {
            return
        }
        this.todos.splice(index, 1);
        this.saveAndUpdateEvents(this.todos);
    },

    search: function() {
        var inputValue = document.getElementById('search_input').value,
            isActive = this.checkActive(),
            filteredTodos = this.todos;

        if (!inputValue) {
            return
        }
        if (isActive) {
            filteredTodos = this.getActiveTodos();
        }
        inputValue.trim().toLowerCase();
        filteredTodos = filteredTodos.filter(function(todo) {
            return todo.title.includes(inputValue);
        });
        document.getElementById('clear_btn').parentNode.removeAttribute('disabled');
        this.saveAndUpdateEvents(filteredTodos);
    },

    clear: function() {
        document.getElementById('clear_btn').parentNode.setAttribute('disabled', 'disabled');
        document.getElementById('search_input').value = '';
        this.saveAndUpdateEvents(this.todos);
    },

    toggle: function(index) {
        if (index === -1) {
            return
        }
        this.todos[index].completed = !this.todos[index].completed;
        var elem = this.todos[index];
        if (elem.completed) {
            this.todos.splice(index, 1);
            this.todos.push(elem);
        }

        this.saveAndUpdateEvents(this.todos);
    },

    toggleActive: function(index) {
        var isActive = this.checkActive();

        if (isActive) {
            document.getElementById('check_active').children[0].innerHTML = 'check_box_outline_blank';
        } else {
            document.getElementById('check_active').children[0].innerHTML = 'check_box';
        }
    },

    checkActive: function() {
        var checkActive = document.getElementById('check_active'),
            inner = checkActive.children[0].innerHTML;
        if (inner === 'check_box') {
            return true;
        } else {
            return false;
        }
    },

    save: function() {
        localStorage.setItem('todos', JSON.stringify(this.todos));
    },

    getActiveTodos: function() {
        return this.todos.filter(function(todo) {
            return !todo.completed;
        });
    },

    getCompletedTodos: function() {
        return this.todos.filter(function(todo) {
            return todo.completed;
        });
    },

    getIndex: function(id) {
        return this.todos.findIndex(function(el) {
            return el.id === id;
        })
    },

    uuid: function() {
        var i, random;
        var uuid = '';

        for (i = 0; i < 32; i++) {
            random = Math.random() * 16 | 0;
            if (i === 8 || i === 12 || i === 16 || i === 20) {
                uuid += '-';
            }
            uuid += (i === 12 ? 4 : (i === 16 ? (random & 3 | 8) : random)).toString(16);
        }

        return uuid;
    },

    render: function(todos) {
        if (!todos) {
            return
        }
        var list = document.getElementById('todo_list')
        while (list.firstChild) {
            list.removeChild(list.firstChild);
        }

        todos.forEach(function(todo) {
            var li = document.createElement('li');
            if (!todo.completed) {
                li.setAttribute('draggable', true);
                li.className = 'drag';
            }
            li.setAttribute('id', 'todo_item-' + todo.id);

            var checkboxButton = document.createElement('button');
            checkboxButton.className = 'checkbox icon';

            var iCheck = document.createElement('i');
            iCheck.className = 'material-icons';
            iCheck.innerHTML = todo.completed ? 'check_box' : 'check_box_outline_blank';
            iCheck.setAttribute('id', todo.id);

            var span = document.createElement('span');
            span.className = todo.completed ? 'title checked' : 'title';
            span.innerHTML = todo.title;

            var div = document.createElement('div');
            div.className = 'actions';

            var deleteButton = document.createElement('button');
            deleteButton.className = 'delete icon';

            var iDelete = document.createElement('i');
            iDelete.className = 'material-icons';
            iDelete.innerHTML = 'clear';
            iDelete.setAttribute('id', todo.id);

            checkboxButton.appendChild(iCheck);
            deleteButton.appendChild(iDelete);
            div.appendChild(deleteButton);

            li.appendChild(checkboxButton);
            li.appendChild(span);
            li.appendChild(div);
            list.appendChild(li);
        });
    }
}

App.init();